package com.sifei.seed;

import java.util.List;

import com.sifei.seed.model.AppMusicSong;
import com.sifei.seed.model.AppMusicSongDao;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Ignore
public class TestJdbcTemplate {
    @Autowired // 将Spring管理的对象注入进来
    private AppMusicSongDao appMusicSongDao;

    @Test
    @Ignore
    public void testGetSecodeHandGoodsList() throws Exception {
        List<AppMusicSong> secondHandGoodsList = appMusicSongDao.getSongList(10, 1);
        System.out.println(secondHandGoodsList);
        Assert.assertNotNull(secondHandGoodsList);
    }

    @Test
    @Ignore
    public void testGetSecodeHandGoodsByGoodsid() throws Exception {
        AppMusicSong secondHandGoods = appMusicSongDao.getSongBysongid(1001);
        System.out.println(secondHandGoods);
        Assert.assertNotNull(secondHandGoods);
    }
}
