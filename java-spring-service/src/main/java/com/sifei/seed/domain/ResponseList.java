package com.sifei.seed.domain;

import java.util.List;

import com.sifei.seed.model.AppMusicSinger;

public class ResponseList {
    private Integer total;
    private List<AppMusicSinger> list;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<AppMusicSinger> getList() {
        return list;
    }

    public void setList(List<AppMusicSinger> list) {
        this.list = list;
    }
}