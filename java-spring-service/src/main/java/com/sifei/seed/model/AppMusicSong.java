package com.sifei.seed.model;

public class AppMusicSong {
    private Integer songid;
    private Integer singerid;
    private String singerName;
    private String fileUrl;
    private String lyric;
    private String type;
    private String songName;
    private String songlink;

    public Integer getSongid() {
        return songid;
    }

    public void setSongid(Integer songid) {
        this.songid = songid;
    }

    public Integer getSingerid() {
        return singerid;
    }

    public void setSingerid(Integer singerid) {
        this.singerid = singerid;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getLyric() {
        return lyric;
    }

    public void setLyric(String lyric) {
        this.lyric = lyric;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSonglink() {
        return songlink;
    }

    public void setSonglink(String songlink) {
        this.songlink = songlink;
    }

    @Override
    public String toString() {
        return "{songid:" + songid + ", singerid:" + singerid + ", songName:" + songName + ", singerName:" + singerName + ", typeid:" + type + ", songlink: " + songlink +  "}";
    }
}