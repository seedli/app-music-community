package com.sifei.seed.controller;

import java.util.List;

import com.sifei.seed.model.AppMusicSong;
import com.sifei.seed.model.AppMusicSongDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller // Spring框架扫描本注解会将类的对象实例化并管理
public class AppMusicSongController {
    @Autowired // 通过Spring框架自动注入对象
    private AppMusicSongDao appMusicSongDao;

    @GetMapping("/song") // 绑定接口地址
    @ResponseBody // 将返回值序列化为应答内容
    public List<AppMusicSong> getAppSecondGoodsList(
        @RequestParam(value="songName", required=false) String songName,
        @RequestParam(value="singerName", required=false) String singerName,
        @RequestParam(value="pageNum", required=false, defaultValue="1") Integer pageNum,
        @RequestParam(value="pageSize", required=false, defaultValue="10") Integer pageSize
    ) {
        List<AppMusicSong> appMusicSongList = appMusicSongDao.getSongList(pageSize, pageNum);
        return appMusicSongList;
    }

    @GetMapping("/song/{songid}") // 绑定接口地址，支持获取地址参数
    @ResponseBody // 将返回值序列化为应答内容
    public AppMusicSong getAppMusicSongBysongsid(
        @PathVariable Integer songid // 从接口地址获取请求参数
        ) {
        AppMusicSong appMusicSong = appMusicSongDao.getSongBysongid(songid);
        return appMusicSong;
    }

//    @RequestMapping(path="/song", method=RequestMethod.POST) // 绑定接口地址，支持获取地址参数
//    @ResponseBody // 将返回值序列化为应答内容
//    public AppMusicSong createAppSecondGoods(
//        @RequestBody AppMusicSong newInfo) {
//        newInfo.setGoodsid(System.currentTimeMillis());
//        appMusicSongDao.createSecodeHandGoodsByGoodsid(newInfo);
//        return newInfo;
//    }
//
//    @RequestMapping(path="/song/{songid}", method=RequestMethod.POST) // 绑定接口地址，支持获取地址参数
//    @ResponseBody // 将返回值序列化为应答内容
//    public AppMusicSong modifyAppSecondGoods(
//        @PathVariable Long goodsid, // 从接口地址获取请求参数
//        @RequestBody AppMusicSong newInfo) {
//        newInfo.setGoodsid(goodsid);
//        appMusicSongDao.modifySecodeHandGoodsByGoodsid(newInfo);
//        return newInfo;
//    }
}
