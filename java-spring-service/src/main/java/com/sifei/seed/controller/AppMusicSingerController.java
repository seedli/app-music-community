package com.sifei.seed.controller;

import com.sifei.seed.model.AppMusicSinger;
import com.sifei.seed.model.AppMusicSingerDao;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(value = "/singer")
@RestController
@RequestMapping("/singer")
@Controller // Spring框架扫描本注解会将类的对象实例化并管理
public class AppMusicSingerController {
    @Autowired // 通过Spring框架自动注入对象
    private AppMusicSingerDao appMusicSingerDao;

    @GetMapping() // 绑定接口地址
    @ResponseBody // 将返回值序列化为应答内容
    public List<AppMusicSinger> getAppSecondGoodsList(

            @RequestParam(value="songName", required=false) String songName,
            @RequestParam(value="singerName", required=false) String singerName,
            @RequestParam(value="pageNum", required=false, defaultValue="1") Integer pageNum,
            @RequestParam(value="pageSize", required=false, defaultValue="10") Integer pageSize
    ) {
        List<AppMusicSinger> appMusicSingerList = appMusicSingerDao.getSingerList(pageSize, pageNum);
        return appMusicSingerList;
    }

    @GetMapping("/{singerid}") // 绑定接口地址，支持获取地址参数
    @ResponseBody // 将返回值序列化为应答内容
    public AppMusicSinger getAppMusicSingerBysingerid(
            @PathVariable Integer singerid // 从接口地址获取请求参数
    ) {
        AppMusicSinger appMusicSinger = appMusicSingerDao.getSingerBysingerid(singerid);
        return appMusicSinger;
    }

//    @RequestMapping(path="/song", method=RequestMethod.POST) // 绑定接口地址，支持获取地址参数
//    @ResponseBody // 将返回值序列化为应答内容
//    public AppMusicSong createAppSecondGoods(
//        @RequestBody AppMusicSong newInfo) {
//        newInfo.setGoodsid(System.currentTimeMillis());
//        appMusicSongDao.createSecodeHandGoodsByGoodsid(newInfo);
//        return newInfo;
//    }
//
//    @RequestMapping(path="/song/{songid}", method=RequestMethod.POST) // 绑定接口地址，支持获取地址参数
//    @ResponseBody // 将返回值序列化为应答内容
//    public AppMusicSong modifyAppSecondGoods(
//        @PathVariable Long goodsid, // 从接口地址获取请求参数
//        @RequestBody AppMusicSong newInfo) {
//        newInfo.setGoodsid(goodsid);
//        appMusicSongDao.modifySecodeHandGoodsByGoodsid(newInfo);
//        return newInfo;
//    }
}
