﻿# Host: localhost  (Version: 5.7.26)
# Date: 2022-06-26 14:04:28
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "tb_artist"
#

DROP TABLE IF EXISTS `tb_artist`;
CREATE TABLE `tb_artist` (
  `singerid` int(11) NOT NULL AUTO_INCREMENT,
  `singerName` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `imgURL` varchar(255) DEFAULT NULL,
  `profile` varchar(500) DEFAULT NULL,
  `ensingerName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`singerid`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

#
# Data for table "tb_artist"
#

/*!40000 ALTER TABLE `tb_artist` DISABLE KEYS */;
INSERT INTO `tb_artist` VALUES (100,'王心凌','内陆','https://imgessl.kugou.com/stdmusic/20220519/20220519154312917179.jpg','王心凌（Cyndi Wang），1982年9月5日出生于中国台湾省新竹县，祖籍山东省青岛市，中国台湾女歌手、演员。','Cyndi Wang'),(102,'王力宏','内陆','https://imgessl.kugou.com/stdmusic/20160907/20160907184642845716.jpg',NULL,NULL);
/*!40000 ALTER TABLE `tb_artist` ENABLE KEYS */;

#
# Structure for table "tb_song"
#

DROP TABLE IF EXISTS `tb_song`;
CREATE TABLE `tb_song` (
  `songid` int(11) NOT NULL AUTO_INCREMENT,
  `songName` varchar(50) DEFAULT NULL,
  `singerid` int(11) DEFAULT NULL,
  `fileURL` varchar(100) DEFAULT NULL,
  `lyric` varchar(255) DEFAULT NULL,
  `songlink` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`songid`)
) ENGINE=MyISAM AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8;

#
# Data for table "tb_song"
#

/*!40000 ALTER TABLE `tb_song` DISABLE KEYS */;
INSERT INTO `tb_song` VALUES (1001,'爱你',100,'https://pica.zhimg.com/80/v2-cb09c8704b89877c1749a39d63d35043_720w.jpg','作词 : 陈思宇/谈晓珍/潘瑛/n作曲 : Lee Yong Min/Hwang Se Joon\\n我弹的钢琴都是为了你弹\\n弹了那么久还是觉得浪漫\\n我弹的时候能听到你在唱\\n感觉上你在这\\n跟我一起说话\\n一天到晚 我不停地想\\n ','https://music.163.com/#/song?id=1957708996'),(1002,'唯一',102,'https://imgessl.kugou.com/stdmusic/20210113/20210113165018607365.jpg','作词 : 王力宏\r\n作曲 : 王力宏\r\n编曲 : 王力宏/刘志远\r\n我的天空多么的清晰\r\n透明的承诺是过去的空气\r\n牵着我的手是你\r\n但你的笑容 却看不清\r\n是否一颗星星变了心\r\n从前的愿望 已全都被抛弃\r\n最近我无法呼吸\r\n连自己的影子\r\n都想逃避',NULL);
/*!40000 ALTER TABLE `tb_song` ENABLE KEYS */;

#
# Structure for table "tb_user"
#

DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `songid` int(11) NOT NULL AUTO_INCREMENT,
  `maneger` varchar(30) DEFAULT NULL,
  `pwd` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`songid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "tb_user"
#

/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
